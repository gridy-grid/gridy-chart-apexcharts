# GridyGrid chart component ApexCharts driver

## Installation

```shell
npm i gridy-grid apexcharts gridy-chart-apexcharts gridy-grid-default sk-theme-default
```

and plug it to your page

```html
<script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>
```

then add element to your container or setup it programmatically

```html
    <gridy-grid id="gridyGrid" base-path="/node_modules/gridy-grid/src" sort-field="$.title">
        <gridy-data-source fields='[{ "title": "Price1", "path": "$.price1" },{ "title": "Price2", "path": "$.price2" }]'
                       datasource-type="DataSourceLocal" datapath="$.data"></gridy-data-source>
    
        <gridy-chart dri="apex-charts" type="bar" field-price1="Price1" field-price2="Price2" width="400" height="300"></gridy-chart>
    
    </gridy-grid>
    <script type="module">
        import { GridyGrid } from '/node_modules/gridy-grid/src/gridy-grid.js';
        let data = [];
        for (let i = 0; i < 10; i++) {
            data.push({ price1: 200 * i, price2: 100 * i })
        }
        let grid = document.querySelector('#gridyGrid');
        grid.addEventListener('bootstrap', () => {
            grid.charts[0].addEventListener('skrender', (event) => {
                grid.dataSource.loadData(data);
            });
        });
        customElements.define('gridy-grid', GridyGrid);
    </script>
    
```

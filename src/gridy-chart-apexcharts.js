import { GridyChartDriver } from "../../gridy-grid/src/chart/gridy-chart-driver.js";
import { FieldMappedChart } from "../../gridy-grid/src/chart/impl/field-mapped-chart.js";

import { JsonPath } from '../../sk-core/src/json-path.js';

export class DefaultApexChart extends FieldMappedChart {
    
    async fmtData(data) {
        let datasets = [];
        let min = 0, max = 0;
        let index = 0;
        let colors = [];
        for (let fieldKey of Object.keys(this.fieldMappings)) {
            let field = this.fieldMappings[fieldKey];
            let dsData = [];
            for (let dataItem of data) {
                let onValue = this.jsonPath.query(dataItem, field.path);
                let value = onValue[0];
                if (value === undefined) {
                    value = dataItem;
                }
                if (! isNaN(value) && value < min) {
                    min = value;
                }
                if (! isNaN(value) && value > max) {
                    max = value;
                }
                dsData.push(value);
            }
            if (field.color) {
                colors[index] = field.color;
            }
            let dataSet = { name: field.title, data: dsData };
            Object.assign(dataSet, field);
            datasets.push(dataSet);
            index++;
        }
        let step = (max - min) / 4;
        let options = {
            series: datasets,
            xaxis: {
                categories: [ min, min + step, min + (2 * step), min + (3 * step), max ],
            }
        };
        if (colors.length > 0) {
            options.colors = colors;
        }
        return options;
    }
    
    render(container, options) {
        let chart = new ApexCharts(container, options);
        chart.render();    
        return chart;
    }
}

export class GridyChartApexCharts extends GridyChartDriver {

    
    implByType(type) {				
        return new DefaultApexChart(this.el);
	}
    
	async renderChart(el, data) {

		this.type = el.getAttribute('type');

		this.chartImpl = this.implByType(this.type);
		this.chartImpl.configFromEl(el);

		let fmtedData = await this.chartImpl.fmtData(data);

		let container = el.querySelector('.gridy-chart-container');
		container.innerHTML = '';
        let options = {
            chart: {
                type: el.chartType
            }
        };
        if (el.options) {
            Object.assign(options, el.options);
        }
        Object.assign(options, fmtedData);
		this.chart = this.chartImpl.render(container, options);
        return Promise.resolve(this.chart);
	}
}
